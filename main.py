import cv2
import numpy as np


def counting():
    img_rgb = cv2.imread('picture.png')
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    template = cv2.imread('point.png', 0)
    w, h = template.shape[::-1]

    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.5
    loc = np.where(res >= threshold)

    for pt in zip(*loc[::-1]):  # Visual inspection
        cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
    cv2.imwrite('res.png', img_rgb)

    n = (len(list(zip(*loc))))
    return print('The number of points equal to {}'.format(n))


if __name__ == '__main__':
    counting()
